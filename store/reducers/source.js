export default (state = {}, action) => {
  if(action.type === '/source/RESPONSE') {
    return action.state;
  }
  return {};
};
