import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import { withStyles } from '@material-ui/core/styles';

import IconButton from '@material-ui/core/IconButton';

import Divider from '@material-ui/core/Divider';

import ContactIcon from '@material-ui/icons/Call';
import ComplianceIcon from '@material-ui/icons/CheckCircle';
import TopicsIcon from '@material-ui/icons/TextFields';
import HomeIcon from '@material-ui/icons/Home';
import CustomerExperienceIcon from '@material-ui/icons/SupervisedUserCircle';
import Integrations from '@material-ui/icons/CallMerge';



import MenuIcon from '@material-ui/icons/Menu';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import sayintDarkTheme from '../../src/sayint-dark-theme';
const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: 'flex',
  },
  drawer: {
    
    alignSelf: 'stretch',
    position: 'absolute',
    left: 0,
    // [theme.breakpoints.up('sm')]: {
    //   width: theme.spacing.unit * 7 + 1,
    //   flexShrink: 0,
    // },
    // [theme.breakpoints.up('md')]: {
    //   width: drawerWidth,
    //   flexShrink: 0,
    // },

  },
  drawerMini: {
    overflowX: 'hidden',
    width: theme.spacing.unit * 7 + 1,
  },
  drawerPaper: {
    width: drawerWidth,
    flexShrink: 0,
    minHeight: '100vh',
    [theme.breakpoints.up('lg')]: {
        position: 'relative',
    },
  },
  toolbar: {
    padding: `${theme.spacing.unit * 1}px ${theme.spacing.unit * 2}px`,
    minHeight: 55,
    [theme.breakpoints.up('sm')]: {
        minHeight: 63,
    },
    justifyContent: 'flex-start',
    alignItems: 'center',
    display: 'flex',
  },
  logo: {
    display: 'block',
    margin: 'auto',
  }
 });

 const darkTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main: '#1f263e',
    },
    background: {
      paper: '#1f263e',
      default: '#1f263e',
    }
  },
});
const Navigation = ({ classes, theme }) => {
  const icons = [<HomeIcon />, <ComplianceIcon />, <TopicsIcon />, <ContactIcon />,  <CustomerExperienceIcon />, <Integrations />]
  const drawer = (
       <div>

       </div>
     );
  return (
    <MuiThemeProvider theme={sayintDarkTheme}>
      <nav className={classes.drawer}>
          <Hidden smUp implementation="css">
            <Drawer
              variant="temporary"
              anchor={theme.direction === 'rtl' ? 'right' : 'left'}
              classes={{
                paper: classes.drawerPaper,
                paperAnchorDockedLeft: classes.paperAnchorDockedLeft,
              }}
            >
              {drawer}
            </Drawer>
          </Hidden>
          <Hidden lgUp xsDown implementation="css">
            <Drawer
              classes={{
                paper: classes.drawerMini,
              }}
              variant="permanent"
              open
            >
              {drawer}
            </Drawer>
          </Hidden>
          <Hidden mdDown implementation="css">
            <Drawer
              classes={{
                paper: classes.drawerPaper,
              }}
              variant="permanent"
              open
            >
              {drawer}
            </Drawer>
          </Hidden>
        </nav>
      </MuiThemeProvider>
)
}


export default withStyles(styles,  { withTheme: true })(Navigation);
