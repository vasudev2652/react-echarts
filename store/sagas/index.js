import { put, takeEvery, fork, delay, all,  takeLatest, call, select, take } from 'redux-saga/effects'
import axios from 'axios';
import {getData, JsonFormsState} from '@jsonforms/core';
import { actionTypes as t } from '../reducers/moduleReducer';
const getUrl = url => `/dev/judges/api/${url}`
export function* requestChartData(action) {
	const state = yield select();
	var filters = yield getData(state,"jsonforms");
	console.log(filters,"filters");
	yield put({
		type: t.SELECT_CHART,
		payload:filters.chart
	})
	if(filters.measure){
		yield put({
			type:t.SELECT_MEASURE,
			payload:filters.measure
		})
		const { data: results } = yield call(axios.get, getUrl('stats'), {
			params: JSON.stringify(filters)
		  })
		  yield put({
			  type:t.FETCH_DATA,
			  payload:results.data
		  });
	}
	if(filters.dimension){
		yield put({
			type:t.SELECT_DIMENSION,
			payload:filters.dimension
		});
	}
}
export default function* rootSaga() {
  yield all([
	  yield takeLatest('jsonforms/UPDATE',requestChartData),
	  yield takeLatest('jsonforms/INIT', requestChartData)
  ])

}
