import {createStore, applyMiddleware, compose, combineReducers} from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootReducer from './reducers'
import rootSaga from './sagas'
import { Provider } from 'react-redux';
import { materialCells, materialRenderers } from '@jsonforms/material-renderers';
import { createLogger } from 'redux-logger'
const devtools = (process.browser && window.__REDUX_DEVTOOLS_EXTENSION__)
  ? window.__REDUX_DEVTOOLS_EXTENSION__()
  : f => f
import isFunction from 'lodash/isFunction';
import reduce from 'lodash/reduce';

function configureStore(moduleName, rootReducer, preloadedState, initialActions) {
  /**
   * Recreate the stdChannel (saga middleware) with every context.
   */
  const sagaMiddleware = createSagaMiddleware();
  /**
   * Since Next.js does server-side rendering, you are REQUIRED to pass
   * `preloadedState` when creating the store.
   */
  const logger = createLogger({
      collapsed: (state, action) => {
        if(!action.type)  {
          return false;
        }
        return action.type.includes('jsonforms')
      },
      // diff: true,
  });
  const store = createStore(rootReducer, {
    jsonforms: {
      cells: materialCells,
      renderers: materialRenderers
    }
  },
    applyMiddleware(logger, sagaMiddleware),
  );
  // initialActions.map((item) => {
  //   store.dispatch(item);
  // });
  /**
   * next-redux-saga depends on `sagaTask` being attached to the store.
   * It is used to await the rootSaga task before sending results to the client.
   */
  store.sagaTask = sagaMiddleware.run(rootSaga)
  store.dispatch({
    type: 'SELECT_CHART'
  });
  return store
}
export default configureStore
export const withStore = ( moduleName, reducers, initialState, initialActions ) =>  Component => {
  const store = configureStore( moduleName, reducers, initialState, initialActions );
  return class App extends React.Component {
    render() {
      return (
        <Provider store={store}>
          <Component />
        </Provider>
      )
    }
  }
};

export const asModule = ( moduleId ) => Component => {
  class ModuleComponent extends React.Component {
    componentDidMount() {
    }
    render() {
      return <Component />
    }
  }
  return
};
