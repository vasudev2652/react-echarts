import ReactEcharts from 'echarts-for-react';
import {withStyles} from '@material-ui/core/styles';
import pie from './pie';
import isEmpty from 'lodash/isEmpty';
import horizontal_bar from './horizontal_bar';
import vertical_bar from './vetical_bar';
const charts={
	pie,
	horizontal_bar,
	vertical_bar
}
const filterKeys = ( d, include, exclude ) => {
	if(d){
		return d.filter(item =>
			include.reduce((agg, key) => (agg && !isEmpty(item[key])), true) &&
			exclude.reduce((agg, key) => (agg && isEmpty(item[key])), true)
		  )
	}
}
export default class extends React.Component {
  constructor(props) {
    super(props)
    this._onEvents = {
      'click':this.onChartClick
    }
  }
  onChartClick=(params)=>{
    console.log('chart clicked',this.props.type);
  }
  render() {
	const d = this.props.data || [];
	const data ={
		total:filterKeys(d, [], ['age_group', 'gender', 'locale', 'domain', 'language']),
		languages: filterKeys(d, ['language'], ['age_group', 'locale', 'gender', 'domain']),
		age_group: filterKeys(d, ['age_group'], ['language', 'locale', 'gender', 'domain']),
		gender: filterKeys(d, ['gender'], ['language', 'locale', 'domain', 'age_group']),
		locale: filterKeys(d, ['locale'], ['language', 'domain', 'gender', 'age_group']),
		domain: filterKeys(d, ['domain'], ['language', 'locale', 'gender', 'age_group'])
	}
    const colors = ['#fbf6c3', '#bde0c2', '#bdd3e1', '#a75fa2', '#c878c3'];
    return (
      <ReactEcharts ref={(e) => { this.echarts_react = e; }} notMerge={true}
      onEvents= {this._onEvents} option={charts[this.props.type]({data:data[this.props.measure],dimension:this.props.dimension, measure:this.props.measure})}  />
    )
  }
}
