
const next = require('next')
const app = next({dev:true})
const handler = routes.getRequestHandler(app)
console.log("DEV MODE: ", process.env.NODE_ENV !== 'production');
// With express
const express = require('express')
app.prepare().then(() => {
  express().use(handler).listen(3000)
})
