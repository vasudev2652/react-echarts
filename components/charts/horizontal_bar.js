import isEmpty from 'lodash/isEmpty';
export default (payload)=>{
	console.log(payload,"payload");
	var defaultSource =[
		['score', 'amount', 'product'],
		[89.3, 58212, 'Matcha Latte'],
		[57.1, 78254, 'Milk Tea'],
		[74.4, 41032, 'Cheese Cocoa'],
		[50.1, 12755, 'Cheese Brownie'],
		[89.7, 20145, 'Matcha Cocoa'],
		[68.1, 79146, 'Tea'],
		[19.6, 91852, 'Orange Juice'],
		[10.6, 101852, 'Lemon Juice'],
		[32.7, 20112, 'Walnut Brownie']
	];
	var tempSource ={
		total_durations: payload.data && payload.data.map((obj,index)=>{
			return [
				obj.recording_duration,
				obj[payload.measure]
			]
		}),
		total_recordings: payload.data && payload.data.map((obj,index)=>{
			return [
				obj.total_utterances,
				obj[payload.measure]
			]
		})
	}
	var option = {
		dataset: {
			source: isEmpty(tempSource[payload.dimension])?defaultSource:tempSource[payload.dimension]
		},
		grid: {containLabel: true},
		xAxis: {name: payload.dimension?payload.dimension:'amount'},
		yAxis: {type:'category'},
		visualMap: {
			orient: 'horizontal',
			left: 'center',
			min: 10,
			max: 100,
			text: ['High Score', 'Low Score'],
			// Map the score column to color
			dimension: 0,
			inRange: {
				color: ['#D7DA8B', '#E15457']
			}
		},
		series: [
			{
				type: 'bar',
				encode: {
					// Map the "amount" column to X axis.
					x: payload.measure?payload.measure:'amount',
					// Map the "product" column to Y axis
					y: payload.dimension?payload.dimension:'product'
				}
			}
		]
	};
	return option;
}