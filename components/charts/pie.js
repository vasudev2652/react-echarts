// const filterKeys = ( d, include, exclude ) => {
// 	return d.filter(item =>
// 	  include.reduce((agg, key) => (agg && !isEmpty(item[key])), true) &&
// 	  exclude.reduce((agg, key) => (agg && isEmpty(item[key])), true)
// 	)
// }
export default (payload)=>{
	console.log(payload,"payload");
	const tempData = {
		total_durations: payload.data && payload.data.map(obj=>{
		return{
			name:obj[payload.measure],
			value:obj.recording_duration
		}
	}),
		total_recordings: payload.data && payload.data.map(obj=>{
			return{
				name : obj[payload.measure],
				value: obj.total_utterances
			}
		})
	}
	const defaultData = [
		{value:335, name:'1'},
		{value:310, name:'2'},
		{value:274, name:'3'},
		{value:235, name:'4'},
		{value:400, name:'5'}
	];
	const option = {
		backgroundColor: '#2c343c',
		title: {
			text: payload.measure || 'Pie chart',
			left: 'center',
			top: 20,
			textStyle: {
				color: '#ccc'
			}
		},
		tooltip : {
			trigger: 'item',
			formatter: "{a} <br/>{b} : {c} ({d}%)"
		},
		visualMap: {
			show: false,
			min: 80,
			max: 600,
			inRange: {
				colorLightness: [0, 1]
			}
		},
		series : [
			{
				name:'series',
				type:'pie',
				radius : '55%',
				center: ['50%', '50%'],
				data:payload.dimension?tempData[payload.dimension]:defaultData.sort(function (a, b) { return a.value - b.value; }),
				roseType: 'radius',
				label: {
					normal: {
						textStyle: {
							color: 'rgba(255, 255, 255, 0.3)'
						}
					}
				},
				labelLine: {
					normal: {
						lineStyle: {
							color: 'rgba(255, 255, 255, 0.3)'
						},
						smooth: 0.2,
						length: 10,
						length2: 20
					}
				},
				itemStyle: {
					normal: {
						color: '#c23531',
						shadowBlur: 200,
						shadowColor: 'rgba(0, 0, 0, 0.5)'
					}
				},
				animationType: 'scale',
				animationEasing: 'elasticOut',
				animationDelay: function (idx) {
					return Math.random() * 200;
				}
			}
		]
	};
	return option;
}