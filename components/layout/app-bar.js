import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import NotificationsIcon from '@material-ui/icons/Notifications';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuIcon from '@material-ui/icons/Menu';
import Badge from '@material-ui/core/Badge';
import Fab from '@material-ui/core/Fab';
const styles = theme => {

  return {
  grow: {
    flexGrow: 1,
  },

  appBar: {
    boxShadow: 'none',
    backgroundColor: '#fff',
    [theme.breakpoints.up('sm')]: {
      // marginLeft: theme.spacing.unit * 7,
    },
    [theme.breakpoints.up('lg')]: {
      // marginLeft: '240px'
    },
    width: 'auto',
    flex: '1 1 0',
  },

  textPrimary: {
    fontSize: '10px',
    textTransform: 'capitalize'
  },
  item: {
   padding: theme.spacing.unit,
   borderLeft: '1px solid rgba(0, 0, 0, 0.15)',
   alignSelf: 'stretch',
   display: 'flex',
   alignItems: 'center',
   justifyContent: 'center',
 },
 leftIcon: {
   marginRight: theme.spacing.unit,
 },
 spacer: {
   flex: '1 1 0'
 },
 menuButton: {
   marginRight: 20,
   [theme.breakpoints.up('sm')]: {
     display: 'none',
   },
 },
}
};

function SimpleAppBar(props) {
  const { classes } = props;

  return (

      <AppBar classes={{
        root: classes.appBar,
      }} position="static" color="default">
        <Toolbar>
           <img src="https://apps.datamime.com/logo.png" height="39.5px" style={{
             marginRight: 8,
           }}/>
          <Typography variant="h6" color="inherit">
            DATAMIME
          </Typography>
           <div className={classes.grow} />
        

           <div className={classes.item}>
            <Button variant="text" color="primary" className={classes.button} classes={{
              textPrimary: classes.textPrimary
            }} onClick={() => {
              window && window.keycloak.logout();
            }}>

               Logout
             </Button>
           </div>
        </Toolbar>
      </AppBar>
  );
}

SimpleAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleAppBar);
