import Layout from '../components/layout/index';
import { Provider, connect } from 'react-redux';
import { compose, combineReducers } from 'redux';
import configureStore, { withStore } from '../store/index';
import { jsonformsReducer,Actions } from "@jsonforms/core";
import Grid from '@material-ui/core/Grid';
import { JsonForms } from '@jsonforms/react';
import moduleReducer from '../store/reducers/moduleReducer';
import Chart from '../components/charts';
const styles = theme => ({
	cancel: {
		margin: theme.spacing.unit,
		backgroundColor: 'rgb(225, 0, 80)',
		color: 'white'
	},
	save: {
		margin: theme.spacing.unit,
		backgroundColor: '#2196f3',
		color: 'white'
	}
});
const jsonSchema = {
	"type": "object",
	"properties": {
		"chart": {
			"type": "string",
			"enum": [
				"pie",
				"horizontal_bar",
				"vertical_bar"
			]
		},
		"measure": {
			"type": "string",
			"enum": [
				"domain",
				"age_group",
				"gender",
				"locale"
			]
		},
		"dimension": {
				"type": "string",
				"enum": [
					"total_durations",
					"total_recordings"
				]
			}
	}
}
const uiSchema = {
	"type": "HorizontalLayout",
	"elements": [
		{
			"type": "Control",
			"label": "type",
			"scope": "#/properties/chart"
		},
		{
			"type": "Control",
			"label": "Measure(x-axis)",
			"scope": "#/properties/measure"
		},
		{
			"type": "Control",
			"label": "Dimension",
			"scope": "#/properties/dimension"
		}
	]
};
const data={
	"chart":"pie",
	"measure":"domain",
	"dimension":"total_recordings"
}
class Module extends React.Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
	}
	render() {
		console.log(this.props.charts.chart,"chart type");
		return (
			<Grid style={{
				minHeight: 400,
			}} container spacing={8}>
				<Grid xs={12} item>
					<JsonForms uischema={uiSchema} schema={jsonSchema} data={data}/>
				</Grid>
				<Grid xs={12} item><Chart type={this.props.charts.chart} data={this.props.charts.data} dimension={this.props.charts.dimension} measure={this.props.charts.measure} /></Grid>
				{/* <Grid xs={5} item><UserList /></Grid>
				<Grid xs={7} item><TaskList /></Grid> */}
			</Grid>
		)
	}
}
const mapStateToProps=(state)=>{
	console.log(state,"entire state");
	return {
		charts: state.main.charts,
	}
}
Module = compose(
	connect(mapStateToProps, null)
)(Module);
let store = '';
export default class ModulePage extends React.Component {
	render() {
		const rootReducer = combineReducers({
			jsonforms: jsonformsReducer(),
			main: moduleReducer()
		});
		store = configureStore('chart', rootReducer, {});
		store.dispatch(Actions.init(data,jsonSchema,uiSchema))
		return (
			<Layout>
				{store &&
					<Provider store={store}>
						<Module />
					</Provider>
				}
			</Layout>
		)
	}
};
