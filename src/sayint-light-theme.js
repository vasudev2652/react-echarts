import { createMuiTheme } from '@material-ui/core/styles';

export default createMuiTheme({
  palette: {
    primary: {
      main: '#a75fa2',
    },
    secondary: {
      main: '#bde0c2',
    },
    common: {
      black: '#4e4e4e',
    },
    background: {
      "paper": "#fff",
      "default": "#f5f7fd"
    }
  },
  typography: {
    useNextVariants: true,
  },
});
