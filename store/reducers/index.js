import { combineReducers } from 'redux';
import { Actions, jsonformsReducer, JsonFormsState } from '@jsonforms/core';

const heartBeatReducer = (state = 0, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return state + 1;
    case 'INCREMENT_IF_ODD':
      return (state % 2 !== 0) ? state + 1 : state;
    case 'DECREMENT':
      return state - 1;
    default:
      return state
  }
};



export default combineReducers({
  jsonForms: jsonformsReducer(),
  heartBeat: heartBeatReducer
});


