import AppBar from './app-bar';
import Navigation from './navigation';
import CssBaseline from '@material-ui/core/CssBaseline';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    flexGrow: 1,
    // maxWidth: 1440,
    margin: 'auto',
    display: 'flex',
    alignItems: 'flex-start',
    // backgroundColor: '#fff',
    position: 'relative',
    flexWrap: 'wrap',
  },
  main: {
    width: '100%',
    padding: theme.spacing.unit * 2,

    [theme.breakpoints.up('sm')]: {
      // marginLeft: theme.spacing.unit * 7,
    },
    // [theme.breakpoints.up('lg')]: {
    //   marginLeft: '240px'
    // },
    // position: 'absolute',
    // left: 0,
    // [theme.breakpoints.up('sm')]: {
    //   width: theme.spacing.unit * 7 + 1,
    //   flexShrink: 0,
    // },
    // [theme.breakpoints.up('md')]: {
    //   width: 240,
    //   flexShrink: 0,
    // },
  }
})

const Layout = ({classes, children, color}) => (
  <div className={classes.root}>
    <CssBaseline />
    <AppBar />
    <main className={classes.main} style={{
      backgroundColor: color === "dark" ? '#1f263e' : '',height: 'calc(100vh - 64px)', overflowY: 'scroll',  overflowX: 'hidden'
    }}>
      {children}
    </main>
  </div>
)

export default withStyles(styles)(Layout);
