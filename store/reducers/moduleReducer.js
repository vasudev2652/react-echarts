const initialState = {
  charts:{
    chart:'pie',
    measure:'',
    data:[],
    dimension:''
  }
};
export const actionTypes  = {
  SELECT_MEASURE: "charts/selectMeasure",
  SELECT_DATA: "charts/selectData",
  SELECT_CHART: "charts/selectActiveChart",
  RENDER_CHART: "charts/renderChart",
  SELECT_DIMENSION: "charts/selectDimension",
  FETCH_DATA: "charts/fetchData"
}

const t = actionTypes;
export default module => (state = initialState, action)  => {
  switch(action.type) {
    case t.SELECT_CHART: {
      return {
        ...state,
        charts: {
          ...state.charts,
          chart:action.payload
        }
      }
    }
    case t.SELECT_MEASURE: {
      return {
        ...state,
        charts: {
          ...state.charts,
          measure:action.payload
        }
      }
    }
    case t.SELECT_DIMENSION: {
      console.log(state,"inside select dimension");
      return {
        ...state,
        charts: {
          ...state.charts,
          dimension:action.payload
        }
      }
    }
    case t.FETCH_DATA: {
      return {
        ...state,
        charts: {
          ...state.charts,
          data:[...action.payload]
        }
      }
    }
    case t.RENDER_CHART: {
      return {
        ...state,
        charts: {
          ...state.charts,
          measure: action.payload.measure,
          data: action.payload.data
        }
      }
    }
  }
  return state
};
